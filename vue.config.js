const path = require('path')
module.exports = {
  configureWebpack: {
    resolve: {
      extensions: ['*', '.js', '.vue', '.scss'],
      alias: {
        'style': path.resolve(__dirname, './src/style'),
        'images': path.resolve(__dirname, './src/assets/images'),
        'fonts': path.resolve(__dirname, './src/assets/fonts'),
        'api': path.resolve(__dirname, './src/api'),
        'utils': path.resolve(__dirname, './src/utils')
      }
    }
  },
  baseUrl: undefined,
  outputDir: undefined,
  assetsDir: undefined,
  runtimeCompiler: undefined,
  productionSourceMap: false,
  parallel: undefined,
  css: {
    sourceMap: false
  }
}
