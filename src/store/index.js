import Vue from 'vue'
import Vuex from 'vuex'
import common from './common'
import plp from './plp'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {common, plp}
})
