import axios from 'axios'

const api = (() => (
  axios.create({
    baseURL: process.env.VUE_APP_APIURL,
    'Content-Type': 'application/json'
  })
))()

async function getURLData (url) {
  try {
    const response = await api.get('core/v1/url_meta', {params: { url }})
    return response.data
  } catch (error) {
    throw error
  }
}

async function getNewToken () {
  try {
    const data = {
      app_version: '',
      device_token: 'web',
      device_type: 'web',
      os_version_code: 'web'
    }
    const response = await api.post('/auth/token', data)
    return response.data
  } catch (e) {
    throw e
  }
}
export {api, getURLData, getNewToken}
