import Vue from 'vue'
import ProtalVue from 'portal-vue'
import App from './App.vue'
import router from './routes/index'
import store from './store'
import './registerServiceWorker'
/* import {checkToken} from './utils' */

Vue.use(ProtalVue)
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
/* (async () => {
  try {
    await checkToken()
    new Vue({
      router,
      store,
      render: h => h(App),
      mounted () {
        console.log('Main App Mounted =-=-=New Vue=-=-=-=-')
      }
    }).$mount('#app')
  } catch (error) {
    console.log(error.response)
  }
})() */
