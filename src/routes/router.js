import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import {getURLData} from '../api'
import store from '../store'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior (to, from, savedPosition) {
    // return desired position
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  const { header } = to.meta
  store.dispatch('common/toogleMenu', false)
  store.dispatch('common/updateFixedScroll', false)
  store.dispatch('common/updateHeaderClass', header || 'header_withSearch')
  next()
})
router.beforeResolve((to, from, next) => {
  testUrlData(to, from)
    .then((bool) => {
      if (bool) {
        getURLData(to.path)
          .then((urlData) => {
            const urlMeta = urlData.data
            const routeData = {
              url: to.path,
              foreignKey: urlMeta.foreign_id
            }
            if (urlMeta.action) {
              next(`${urlMeta.action.url}`) // redirect to the url provided
            }
            store.dispatch('common/updateRouteData', routeData)
            store.dispatch('common/updateMetaData', urlMeta.meta_data)
            store.dispatch('common/updateTitle', urlMeta.heading_title)
            /* store.dispatch('common/updateHeaderClass', header || 'header_withSearch') */
            next()
          })
          .catch((error) => {
            console.log(error)
          })
      } else {
        if (to.meta.title) {
          store.dispatch('common/updateTitle', to.meta.title)
        }
        next()
      }
    })
})

function testUrlData (to, from) {
  /* debugger */
  const {url} = store.state.common.routeData
  return new Promise((resolve, reject) => {
    /* debugger */
    if (!to.meta.foreignKey || to.path === url) {
      resolve(false)
    } else if (to.path !== from.path && to.meta.foreignKey) {
      resolve(true)
    }
  })
}

export default router
