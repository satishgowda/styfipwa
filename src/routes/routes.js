/**
 * Every route becomes a chunk, loaded only when used.
 * Reduces size of initial App load.
 */
const routes = [
  {
    name: 'main',
    path: '/',
    component: () => import(/* webpackChunkName: "landing" */ '@/pages/landing/main.vue'),
    meta: { header: 'header_withSearch', title: 'Landing', foreignKey: false },
    title: 'STYFI'
  },
  {
    name: 'MenLanding',
    path: '/:gender(men|women)',
    component: () => import(/* webpackChunkName: "genderLanding" */ '@/pages/genderlanding/main.vue'),
    meta: { header: 'header_withSearch', title: 'Landing', foreignKey: false },
    title: 'STYFI'
  },
  {
    name: 'PLP',
    path: '/:gender(men|women)-:category_slug',
    component: () => import(/* webpackChunkName: "plp" */ '@/pages/plp/main.vue'),
    meta: { header: 'header_backbutton_withSearchIcon', foreignKey: true },
    title: 'STYFI'
  },
  {
    name: 'Curated',
    path: '/curated/:content_slug',
    component: () => import(/* webpackChunkName: "plp" */ '@/pages/plp/curated.vue'),
    meta: { header: 'header_backbutton_withSearchIcon', foreignKey: true },
    title: 'STYFI'
  },
  {
    name: 'PDP',
    path: '/product/:product_slug',
    component: () => import(/* webpackChunkName: "pdp" */ '@/pages/pdp/main.vue'),
    meta: { header: 'header_transparent_backbutton_withShare', foreignKey: true },
    title: 'STYFI'
  },
  {
    name: 'Brands',
    path: '/:gender?/brands',
    component: () => import(/* webpackChunkName: "brandListing" */ '@/pages/brandslisting/main.vue'),
    meta: { header: 'header_backbutton_withSearchIcon', title: 'Brands', foreignKey: false },
    title: 'STYFI'
  },
  {
    name: 'BrandProfile',
    path: '/brand/:brand_name',
    component: () => import(/* webpackChunkName: "brandProfile" */ '@/pages/plp/brands.vue'),
    meta: { header: 'header_backbutton_withSearchIcon', title: 'BrandProfile', foreignKey: true },
    title: 'STYFI'
  },
  {
    name: 'Explore',
    path: '/explore/:gender',
    component: () => import(/* webpackChunkName: "explore" */ '@/pages/explore/main.vue'),
    meta: { header: 'header_burger_withSearchIcon', title: 'Explore', foreignKey: false },
    title: 'STYFI'
  },
  {
    name: 'ReadsList',
    path: '/reads/:gender',
    component: () => import(/* webpackChunkName: "readList" */ '@/pages/reads/main.vue'),
    meta: { header: 'header_burger_withSearchIcon', title: 'ReaderList', foreignKey: true },
    title: 'STYFI'
  },
  {
    name: 'Article',
    path: '/read/:article_slug',
    component: () => import(/* webpackChunkName: "article" */ '@/pages/reads/blogArticle.vue'),
    meta: { header: 'header_backButton_withShareIcon', title: 'Articles', foreignKey: true },
    title: 'STYFI'
  },
  {
    name: 'Collections',
    path: '/collections/:gender(men|women)',
    component: () => import(/* webpackChunkName: "collections" */ '@/pages/collections/main.vue'),
    meta: { header: 'header_burger_withSearchIcon', title: 'Collection', foreignKey: true },
    children: [
      {
        path: ':topic_slug?',
        component: () => import(/* webpackChunkName: "collectionList" */ '@/pages/collections/collectionList.vue'),
        meta: { header: 'header_burger_withSearchIcon', title: 'Collection', foreignKey: true }
      }
    ],
    title: 'STYFI'
  },
  {
    name: 'Looks',
    path: '/looks/:gender(men|women)',
    component: () => import(/* webpackChunkName: "looks" */ '@/pages/looks/main.vue'),
    meta: { header: 'header_burger_withSearchIcon', title: 'Looks', foreignKey: true },
    children: [
      {
        path: ':topic_slug?',
        component: () => import(/* webpackChunkName: "looksList" */ '@/pages/looks/looksList.vue'),
        meta: { header: 'header_burger_withSearchIcon', title: 'Looks', foreignKey: true }
      }
    ],
    title: 'STYFI'
  },
  {
    name: 'LookDetail',
    path: '/looks/:name_slug',
    component: () => import(/* webpackChunkName: "lookdetail" */ '@/pages/lookdetails/main.vue'),
    meta: { header: 'header_transparent_backButton_withOutShare', title: 'Looks', foreignKey: true }
  },
  {
    name: 'CollectionDetail',
    path: '/collections/:name_slug',
    component: () => import(/* webpackChunkName: "collectiondetail" */ '@/pages/plp/collectiondetails.vue'),
    meta: { header: 'header_transparent_backButton_withOutShare', title: 'Looks', foreignKey: true }
  },
  {
    name: 'AutoSuggest',
    path: '/autosuggest',
    component: () => import(/* webpackChunkName: "autosuggest" */ '@/pages/autosuggest/main.vue'),
    meta: { header: 'header_hide', title: 'Looks', foreignKey: true }
  },
  {
    name: 'Search',
    path: '/search',
    component: () => import(/* webpackChunkName: "search" */ '@/pages/plp/search.vue'),
    meta: { header: 'header_backbutton_withSearchIcon', title: 'Search', foreignKey: true },
    title: 'STYFI'
  }
]
export default routes
