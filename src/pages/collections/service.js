import { api } from '../../api'

export default {
  getCollectionTopics (gender) {
    return api.get('/core/v2/explore/topic/list', {params: {gender, type: 'collections'}})
  },
  getCollectionsList (gender, page, id) {
    return api.get('/core/v2/explore/collection/curated', {params: {topic_id: id, gender, page}})
  }
}
