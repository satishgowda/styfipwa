import {api} from '../../api'

export default {
  getProductDetails (productId) {
    return api.get('core/v3/product/detail',
      {
        params: {'product_id': productId},
        transformResponse (response) {
          const {errors, data} = JSON.parse(response)
          let transformData
          if (errors.length) {
            transformData = errors
          } else {
            transformData = data
          }
          return transformData
        }
      })
  },
  getSimilarProducts (prodId, brandId) {
    return api.get('/core/v2/category/similar_products', {params: {product_id: prodId, brand_id: brandId}})
  },
  getSimilarBrandProducts (prodId, brandId) {
    return api.get('/core/v2/brand/similar_products', {params: {product_id: prodId, brand_id: brandId}})
  },
  getSizeChart (prodId, brandId, categoryId) {
    return api.get('/core/v2/product/sizechart', {params: {product_id: prodId, brand_id: brandId, category_id: categoryId}})
  },
  wishlistProduct (prodData) {
    return api.post('/profile/v2/wishlist/customer', prodData)
  }
}
