import { api } from '../../api'

export default {
  getBrandsList (genderId) {
    return api.get('api/brands', {
      params: {gender_id: genderId}
    })
  }
}
