import {api} from '../../api'

export default {
  getLooksTopics (gender) {
    return api.get('/core/v2/explore/topic/list', {params: {gender, type: 'looks'}})
  },
  getLooksList (gender, page, id) {
    return api.get('/core/v2/explore/look', {params: {topic_id: id, gender, page}})
  }
}
