import { api } from '../../api'

export default {
  getExploreListing (gender) {
    return api.get('/core/v2/explore/overview-non-blog', {
      params: {gender: gender}
    })
  },
  getBlogList (gender) {
    return api.get('/core/v2/explore/overview-blog', {
      params: {gender: gender}
    })
  }
}
