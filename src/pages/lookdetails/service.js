import {api} from '../../api'

export default {
  getLookDetails (id) {
    return api.get('/core/v2/explore/look/details', {params: {id}})
  },
  getLookProducts (id) {
    return api.get('/core/v2/look/products', {params: {look_id: id}})
  }
}
