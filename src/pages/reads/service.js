import { api } from '../../api'

export default {
  getBlogArticlesList (gender) {
    return api.get('/core/v2/explore/blog/list', {
      params: { gender }
    })
  },
  getBlogArticle (id) {
    return api.get('/core/v2/explore/blog', {
      params: { id }
    })
  }
}
